#!/usr/bin/env ruby
# Reference: http://queirozf.com/entries/selenium-webdriver-with-ruby-basic-usage-example
# load the required gems
gem 'selenium-webdriver'
require 'selenium-webdriver'

puts "Setting up the remote selenium driver"
driver = Selenium::WebDriver.for :remote, url: "http://localhost:4444/wd/hub", desired_capabilities: :chrome

# Allow Zalenium to spin up an elgalu/docker-selenium container
sleep 5


puts "Visit google"
driver.navigate.to "http://www.google.com"

puts "google \"football\""
driver.find_element(:css,"form input[type='text']").send_keys("football\n")

puts "wait until \"Wikipedia\" appears in the google search. Timeout after 5sec."
wait = Selenium::WebDriver::Wait.new(:timeout => 5)
wait.until { driver.page_source.include?("Wikipedia") }

at_exit { driver.quit }
